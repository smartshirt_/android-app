package app.myfirstapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;


import org.apache.http.client.HttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static android.provider.UserDictionary.Words.APP_ID;

public class MyActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private int HREN;
    private BluetoothAdapter btAdapter;
    private Handler mHandler;
    private boolean mScanning;
    private BluetoothLeService btLeService;
    private BluetoothGatt btGatt;
    private String deviceAddress;
    private boolean called = false;
    private GoogleApiClient googleApiClient;
    private double currentLat, currentLon;
    private String bpm, steps, temperature;

    private String stepCountSerivceUUID = "00001814-0000-1000-8000-00805f9b34fb";
    private String heartRateServiceUUID = "0000180d-0000-1000-8000-00805f9b34fb";
    private String temperatureServiceUUID  = "00001809-0000-1000-8000-00805f9b34fb";
    private String bodyPositionServiceUUID = "931c1539-3ac4-4354-be88-8d77b22d1e3a";

    private String heartRateCharUUID = "49d1f43c-8f1c-4a78-a83f-cdb81da0052d";
    private String stepCounterCharUUID = "a676dc00-882b-406e-bbce-7feb6978dcac";
    private String temperatureCharUUID = "04f46f5e-252e-4974-b0b1-3afc6bd1a23a";
    private String bodyPositionCharUUID = "305d36fd-cf59-480b-affe-47fa65ac0c0c";

    private Queue<BluetoothGattDescriptor> descriptorWriteQueue = new LinkedList<BluetoothGattDescriptor>();
    private Queue<BluetoothGattCharacteristic> characteristicReadQueue = new LinkedList<BluetoothGattCharacteristic>();

    @Override
    public void onCreate(Bundle savedInstanceSatate) {
        super.onCreate(savedInstanceSatate);
        setContentView(R.layout.activity_my);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    HREN);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET},
                    HREN);
        }

        googleApiClient = new GoogleApiClient.Builder(this, this,  this).addApi(LocationServices.API).build();




        final BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mHandler = new Handler();
        btAdapter = btManager.getAdapter();



        scanLeDevice(true);

    }



    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(MyActivity.class.getSimpleName(), "Can't connect to Google Play Services!");
    }


    public void onConnected(Bundle bundle) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            currentLat = lastLocation.getLatitude();
            currentLon = lastLocation.getLongitude();
            String units = "imperial";
            Log.i(String.format("%f", currentLat), String.format("%f", currentLon));

        }
    }

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            btLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!btLeService.initialize()) {
                Log.e("", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            btLeService.connect(deviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            btLeService = null;
        }
    };

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    btAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, 10000);

            mScanning = true;
            btAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            btAdapter.stopLeScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (device.toString().equals("98:4F:EE:0F:33:12")) {
                        btGatt = device.connectGatt(MyActivity.this, true, gattCallback);
                        scanLeDevice(false);
                    }
                }
            });
        }

    };


    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {


        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState) {
            gatt.discoverServices();
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                List<BluetoothGattService> services = gatt.getServices();

                for (BluetoothGattService service : services) {
//                    Log.i("service", service.getUuid().toString());
                    List<BluetoothGattCharacteristic> chars = service.getCharacteristics();


                    String currentServiceUUID = service.getUuid().toString().toLowerCase();


                    if (
                            currentServiceUUID.equals(stepCountSerivceUUID) || currentServiceUUID.equals(heartRateServiceUUID) || currentServiceUUID.equals(temperatureServiceUUID) || currentServiceUUID.equals(bodyPositionServiceUUID)
                            ) {

                        chars = service.getCharacteristics();

                        for (BluetoothGattCharacteristic characteristic : chars) {

                            if (!characteristicReadQueue.contains(characteristic)) {
                                Log.i("char", characteristic.getUuid().toString());
                                characteristicReadQueue.add(characteristic);
                            }



                        }
                    }

                }

                do_some_fucking_shit(gatt);
            }
        }

        public void do_some_fucking_shit(BluetoothGatt gatt) {


                BluetoothGattCharacteristic current = characteristicReadQueue.element();
                gatt.readCharacteristic(current);
                Log.i("hren", "hren2");

        }

        @Override
        // Result of a characteristic read operation
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            gatt.setCharacteristicNotification(characteristic, true);
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);

        }


        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (characteristicReadQueue.size() > 0)
            characteristicReadQueue.remove();


            if (!characteristicReadQueue.isEmpty())
                do_some_fucking_shit(gatt);
        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            String currentUUID = characteristic.getUuid().toString().toLowerCase();
            Log.d("something changed","");
            if (currentUUID.equals(stepCounterCharUUID)) {
                Log.d("", "step counter");
                final byte[] data = characteristic.getValue();

                if (data != null && data.length > 0) {
                    ￼Sandufinal StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data)
                        stringBuilder.insert(0, String.format("%02X", byteChar));

                    Log.d("received", stringBuilder.toString());
                    String hexString = stringBuilder.toString();

                    final String decimalString = String.format("%d", Long.parseLong(hexString, 16));
                    Log.d("received", decimalString);

                    final TextView steps = (TextView) findViewById(R.id.steps);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            steps.setText(decimalString);
                        }
                    });
                } else {
                    Log.d("", "nothing to read");
                }
            }


            if (currentUUID.equals(heartRateCharUUID)) {
                final byte[] data = characteristic.getValue();
                if (data!=null && data.length > 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data) {
                        stringBuilder.insert(0, String.format("%02X", byteChar));
                    }

                    Log.d("received", stringBuilder.toString());
                    String hexString = stringBuilder.toString();

                    final String decimalString = String.format("%d", Long.parseLong(hexString, 16));
                    Log.d("received", decimalString);

                    final TextView bpmText = (TextView) findViewById(R.id.bpm);

                    bpm = decimalString;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            bpmText.setText(decimalString);
                        }
                    });
                }

            }


            if (currentUUID.equals(temperatureCharUUID)) {
                final byte[] data = characteristic.getValue();
                if (data!=null && data.length > 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data) {
                        stringBuilder.insert(0, String.format("%02X", byteChar));
                    }


                    String hexString = stringBuilder.toString();

                    final int temperatureInInt = Integer.parseInt(hexString, 16);
                    float temperatureFloat = Float.intBitsToFloat(temperatureInInt);

                    NumberFormat formattedTemperature = NumberFormat.getNumberInstance();
                    formattedTemperature.setMinimumFractionDigits(1);
                    formattedTemperature.setMaximumFractionDigits(1);


                    final String temperatureString = formattedTemperature.format(temperatureFloat);

                    temperature = temperatureString;


                    final TextView temperatureTitle = (TextView) findViewById(R.id.temperatureTitle);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            temperatureTitle.setText(temperatureString);
                        }
                    });
                }
            }


            if (currentUUID.equals(bodyPositionCharUUID)) {
                final byte[] data = characteristic.getValue();
                if (data!=null && data.length > 0) {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for (byte byteChar : data) {
                        stringBuilder.insert(0, String.format("%02X", byteChar));
                    }


                    String hexString = stringBuilder.toString();

                    final String decimalString = String.format("%d", Long.parseLong(hexString, 16));
                    if (decimalString.equals("1") && called == false) {
                        called= true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                    called = true;
                                    final Timer timer = new Timer();
                                    timer.schedule(new TimerTask() {
                                        public void run(){

                                            HttpClient httpClient = new DefaultHttpClient();
                                            HttpPost httpPost = new HttpPost("http://shirt.pythonanywhere.com/status/");
                                            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

                                            nameValuePair.add(new BasicNameValuePair("bpm", bpm ));

                                            String coords = String.format("%f,%f", currentLat, currentLon);

                                            nameValuePair.add(new BasicNameValuePair("coords", coords));

                                            nameValuePair.add(new BasicNameValuePair("temperature", temperature));
                                            httpPost.setHeader(HTTP.CONTENT_TYPE,
                                                    "application/x-www-form-urlencoded;charset=UTF-8");

                                            try {
                                                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                                            } catch(UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }

                                            try {
                                                HttpResponse response = httpClient.execute(httpPost);
                                                Log.d("http post response", EntityUtils.toString(response.getEntity()));
                                            } catch (ClientProtocolException e) {
                                                e.printStackTrace();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            timer.cancel();
                                        }
                                    }, 5000);
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(MyActivity.this);
                                            builder.setTitle("Press the OK button if you're ok.");
                                            builder.setMessage("кто у нас там грохнулся?");
                                            builder.setCancelable(true);
                                            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Log.i("lat", String.format("%f", currentLat));
                                                    timer.cancel();

                                                    called=false;

                                                }
                                            });
                                            builder.create();
                                            builder.show();



                            }
                        });
                    }
                }

            }
        }







    };

}

